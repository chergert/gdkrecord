#!/bin/sh

srcdir=`dirname $0`
test -z "$srcdir" && srcdir=.

test -f "configure.ac" || {
        echo "You must run this script in the top-level gdkrecord directory"
        exit 1
}

autoreconf -v --install || exit $?

./configure --enable-maintainer-mode "$@" && echo "Now type 'make' to compile gdkrecord"
