/* gdkrecord.c
 * 
 * Copyright (C) 2009 Christian Hergert
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <unistd.h>
#include <string.h>
#include <glib.h>
#include <gtk/gtk.h>

#define LOG_FUNC(f) ((EventLogFunc)f)

/* This GTK Module is for recording various Gdk events and timers for
 * analysis.  The goal is to make it easier for myself to find hot-spots
 * as well as inconsistancies between backends.
 *
 * If it is useful for you, then awesome :-)
 *
 * The logging format is kept in an ascii format, even for numbers, so
 * that it is trivial to parse with external text processing languages
 * such as python.
 *
 * For example:
 *
 *   START<double>|STOP<double>|X_TIME_MS<int>|TYPE<int>|SIZE<int>|BUFFER<char*>
 *   0.1234|0.1344|1230|7|14|thisissomedata
 *
 * - XTime is the time the server originated event in ms
 * - indicates an event type of 7.
 * - a buffer of 14 following the pipe.
 * - the string is not null terminated.
 * - the next event follows immediately after.
 */

typedef void (*EventLogFunc) (GdkEvent *event, gdouble begin, gdouble end);

static GIOChannel *channel = NULL;
static GTimer     *timer   = NULL;

static gdouble
server_time_for_event (GdkEvent *event)
{
	return 0.0;
}

static void
gdkrecord_event_log (gpointer     event,
                     gdouble      begin,
                     gdouble      end,
                     const gchar *info)
{
	gchar *buffer;

	buffer = g_strdup_printf ("%f|%f|%f|%d|%d|%s",
	                          begin, end,
	                          server_time_for_event (event),
	                          ((GdkEvent*)event)->type,
	                          (gint)strlen (info), info);
	g_io_channel_write_chars (channel, buffer, strlen (buffer), NULL, NULL);
	g_io_channel_flush (channel, NULL);
	g_free (buffer);
}

static void
gdkrecord_event_expose (GdkEventExpose *event,
                        gdouble         begin,
                        gdouble         end)
{
	gdkrecord_event_log (event, begin, end, "");
}

static void
gdkrecord_event_crossing (GdkEventCrossing *event,
                          gdouble           begin,
                          gdouble           end)
{
	gdkrecord_event_log (event, begin, end, "");
}

static void
gdkrecord_event_window_state (GdkEventWindowState *event,
                              gdouble              begin,
                              gdouble              end)
{
	gdkrecord_event_log (event, begin, end, "");
}

static void
gdkrecord_event_visibility (GdkEventVisibility *event,
                            gdouble             begin,
                            gdouble             end)
{
	gdkrecord_event_log (event, begin, end, "");
}

static void
gdkrecord_event_button (GdkEventButton *event,
                        gdouble         begin,
                        gdouble         end)
{
	gdkrecord_event_log (event, begin, end, "");
}

static void
gdkrecord_event_map (GdkEvent *event,
                     gdouble   begin,
                     gdouble   end)
{
	gdkrecord_event_log (event, begin, end, "");
}

static void
gdkrecord_event_unmap (GdkEvent *event,
                       gdouble   begin,
                       gdouble   end)
{
	gdkrecord_event_log (event, begin, end, "");
}

static void
gdkrecord_event_key (GdkEventKey *event,
                     gdouble      begin,
                     gdouble      end)
{
	const gchar *name;

	name = gdk_keyval_name (event->keyval);
	gdkrecord_event_log (event, begin, end, name);
}

static void
gdkrecord_event_handler (GdkEvent *event,
                         gpointer  data)
{
	EventLogFunc log_func = NULL;
	gdouble      begin,
		     end;

	switch (event->type) {
	case GDK_EXPOSE:
		log_func = LOG_FUNC (gdkrecord_event_expose);
		break;
	case GDK_VISIBILITY_NOTIFY:
		log_func = LOG_FUNC (gdkrecord_event_visibility);
		break;
	case GDK_WINDOW_STATE:
		log_func = LOG_FUNC (gdkrecord_event_window_state);
		break;
	case GDK_LEAVE_NOTIFY:
	case GDK_ENTER_NOTIFY:
		log_func = LOG_FUNC (gdkrecord_event_crossing);
		break;
	case GDK_BUTTON_PRESS:
	case GDK_2BUTTON_PRESS:
	case GDK_3BUTTON_PRESS:
	case GDK_BUTTON_RELEASE:
		log_func = LOG_FUNC (gdkrecord_event_button);
		break;
	case GDK_MAP:
		log_func = LOG_FUNC (gdkrecord_event_map);
		break;
	case GDK_UNMAP:
		log_func = LOG_FUNC (gdkrecord_event_unmap);
		break;
	case GDK_KEY_PRESS:
	case GDK_KEY_RELEASE:
		log_func = LOG_FUNC (gdkrecord_event_key);
		break;
	case GDK_CLIENT_EVENT:
	case GDK_SELECTION_CLEAR:
	case GDK_SELECTION_REQUEST:
	case GDK_SELECTION_NOTIFY:
	case GDK_PROXIMITY_IN:
	case GDK_PROXIMITY_OUT:
	case GDK_DRAG_ENTER:
	case GDK_DRAG_LEAVE:
	case GDK_DRAG_MOTION:
	case GDK_DRAG_STATUS:
	case GDK_DROP_START:
	case GDK_DROP_FINISHED:
	case GDK_DELETE:
	case GDK_DESTROY:
	case GDK_MOTION_NOTIFY:
	case GDK_FOCUS_CHANGE:
	case GDK_CONFIGURE:
	case GDK_PROPERTY_NOTIFY:
	case GDK_NO_EXPOSE:
	case GDK_SCROLL:
	case GDK_SETTING:
	case GDK_OWNER_CHANGE:
	case GDK_GRAB_BROKEN:
	case GDK_DAMAGE:
	default:
		break;
	}

	begin = g_timer_elapsed (timer, NULL);
	gtk_main_do_event (event);
	end = g_timer_elapsed (timer, NULL);

	if (G_LIKELY (log_func))
		log_func (event, begin, end);
}

static void
gdkrecord_event_destroy (gpointer data)
{
	if (channel != NULL) {
		g_io_channel_unref (channel);
		channel = NULL;
	}
}

gint
gtk_module_init (gint   argc,
                 gchar *argv[])
{
	gchar      *filename;
	GError     *error = NULL;

	/* get our destination filename */
	if (g_getenv ("GDKRECORD_FILENAME"))
		filename = g_strdup (g_getenv ("GDKRECORD_FILENAME"));
	else
		filename = g_strdup_printf ("%d-%s.gdkrecord",
		                            getpid (),
		                            g_get_prgname ());

	/* open the file for writing */
	if (!(channel = g_io_channel_new_file (filename, "w", &error))) {
		g_warning ("Could not initialize gdkrecord: %s",
		           error->message);
		g_error_free (error);
		g_free (filename);
		return -1;
	}

	g_free (filename);

	/* setup our timer for event timings */
	timer = g_timer_new ();

	/* register our event handler to record and then pass to gtk */
	gdk_event_handler_set (gdkrecord_event_handler,
	                       channel,
	                       gdkrecord_event_destroy);

	return 0;
}

